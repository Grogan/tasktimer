var args = arguments[0];

function addTask() {
    var tasks01 = Alloy.Collections.Tasks01;

    // Create a new model for the task collection (using the type passed in)
    var newTask = Alloy.createModel("Task01", {Type: args.type, Mode: 0, Description: $.taskField.value});
    
    // add new model to the global collection
    tasks01.add(newTask);

    // save the model to persistent storage
    newTask.save();

    // reload the tasks
    tasks01.fetch();

    closeWindow();
}

function focusTextField() {
    $.taskField.focus();
}

function closeKeyboard(e) {
    e.source.blur();
}

function closeWindow() {
    $.addWindow.close();
};