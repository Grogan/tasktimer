//var taskLog01 = Alloy.Collection.TaskLogs01;
var args = arguments[0] || {};
//$.field.rowType = args.rowType;

var tasks01 = Alloy.Collections.Tasks01;
var id;

// $model represents the current model accessible to this 
// controller from the markup's model-view binding. $model
// will be null if there is no binding in place. 

if ($model) {
	id = $model.id;
		
	switch ($model.get("Mode")) {
		case 0: // active can play
			$.row.backgroundColor = "#fff";
			$.activeTask.backgroundColor = "#fff";
			$.task.color = "#000";
			//$.mode.image = "/aplay.png";
			$.activeTask.value = false;
			$.activeTask.enabled = true;
			break;
		
		case 1: // active can stop
			$.row.backgroundColor = "#fff";
			$.activeTask.backgroundColor = "#fff";
			$.task.color = "#000";
			//$.mode.image = "/astop.png";
			$.activeTask.value = true;
			$.activeTask.enabled = true;
			break;
			
		case 2: // inactive
			$.row.backgroundColor = "#eee";
			$.activeTask.backgroundColor = "#eee";
			$.task.color = "#ccc";
			//$.mode.image = "/inactive.png";
			//$.mode.image = "/apause.png";
			$.activeTask.value = false;
			$.activeTask.enabled = false;
			break;
			
		default:
			setMode0(id);
			break;
	}
}

// change the "Mode" status of the IDed task
function changeMode(e) {
	// find the todo task by id
	var task = tasks01.get(id);

	// set the current "done" and "date_completed" fields for the model,
	// then save to presistence, and model-view binding will automatically
	// reflect this in the tableview

	switch ($model.get("Mode")) {
		case 0: // active can play
			//Alloy.Collections.Tasks01.changeModeByType(2,1);
			tasks01.changeModeByType(2,args.rowType);
    		tasks01.get(id).set({Mode : 1}).save();
			break;
		
		case 1: // active can stop
			tasks01.changeModeByType(0,args.rowType);
    		//tasks01.get(id).set({Mode : 0}).save();
			break;
			
		case 2: // inactive, do nothing
			break;
			
		default:
			break;
	}
	
	// reload the chaged data into the collection
	tasks01.fetch();	
}

// delete the IDed task from the collection
function deleteTask(e) {
	// prevent bubbling up to the row
	e.cancelBubble = true;

	if (!tasks01.isAnyTaskActiveByType(args.rowType))
	{
		// if no tasks are active then delete is enabled
		
		// find the todo task by id
		var task = tasks01.get(id);
	
		// destroy the model from persistence, which will in turn remove
		// it from the collection, and model-view binding will automatically
		// reflect this in the tableview
		task.destroy();
	}	
}