exports.definition = {
    config : {
        // table schema and adapter information
	    columns: {
	        Id: "integer primary key autoincrement",
	        MemberId: "text",
	        GroupId: "text",
	        Type: "integer",
	        Description: "text",
	        StartTime: "text",
	        EndTime: "text"
	    },
	    adapter: {
	        type: "sql",
	        collection_name: "TaskLog01",
	        idAttribute: "TaskLogId"
	    }
    },

    extendModel: function(Model) {		
        _.extend(Model.prototype, {
            // Extend, override or implement Backbone.Model 
        });
		
        return Model;
    },

    extendCollection: function(Collection) {		
        _.extend(Collection.prototype, {
            // Extend, override or implement Backbone.Collection
            createTimeEntry : function(description, type) {
                var collection = this;
                var util = require("alloy/sync/util");
                var dbName = collection.config.adapter.db_name;
                var table = collection.config.adapter.collection_name;
  
                db = Ti.Database.open(dbName);
                
	            var moment = require("alloy/moment");                

                // we are looking for any active rows ie Mode 1                
                var sqlInsert = "INSERT INTO " + table + "(description, type, startTime) VALUES (" + description + "," + type + "," + moment.utcNow + ");";
 
                db.execute(sqlInsert);
                
                db.close(); 
            },
            
            stopTimeEntry : function() {
                var collection = this;
                var util = require("alloy/sync/util");
                var dbName = collection.config.adapter.db_name;
                var table = collection.config.adapter.collection_name;
  
                db = Ti.Database.open(dbName);
                
	            var moment = require("alloy/moment");  
	            // only stopping and starting one at a time at this stage
	            // if it is required to do mutiple, then more work is required here              
                sqlInsert = "UPDATE " + table + " SET EndTime = " + moment.utcNow + " WHERE EndTime = NULL;";
                db.execute(sqlInsert);
                
                db.close(); 
            }
        });
		
        return Collection;
    }
}