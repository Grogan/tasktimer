exports.definition = {
    config : {
        // table schema and adapter information
	    columns: {
	    	TaskId: "integer primary key autoincrement",
	        Type: "integer",
	        Mode: "integer",
	        Description: "text",
	        Usages: "integer"
	    },
	    adapter: {
	        type: "sql",
	        collection_name: "Task01",
	        idAttribute: "TaskId"
	    }
    },

    extendModel: function(Model) {		
        _.extend(Model.prototype, {
            // Extend, override or implement Backbone.Model 
        });
		
        return Model;
    },

    extendCollection: function(Collection) {		
        _.extend(Collection.prototype, {
            // Extend, override or implement Backbone.Collection 
            changeModeByType : function(mode, type) {
                var collection = this;
                
                var util = require("alloy/sync/util");
 
                var dbName = collection.config.adapter.db_name;
                var table = collection.config.adapter.collection_name;
  
                db = Ti.Database.open(dbName);
                                
                var sqlInsert = "UPDATE " + table + " SET Mode = " + mode + " WHERE Type = " + type + ";";
 
                db.execute(sqlInsert);
                
                db.close();
 
                //collection.trigger('sync');
            },
            
            isAnyTaskActiveByType : function(type) {
                var collection = this;
                var util = require("alloy/sync/util");
                var dbName = collection.config.adapter.db_name;
                var table = collection.config.adapter.collection_name;
  
                db = Ti.Database.open(dbName);
                                
                // we are looking for any active rows ie Mode 1                
                var sqlInsert = "SELECT * FROM " + table + " WHERE Mode = 1 AND Type = " + type + ";";
 
                var rows = db.execute(sqlInsert);
                // true if there are any valid rows
                var result = rows.isValidRow();
                
                db.close(); 
                rows.close();
                
                return result;           	
            }
        });
		
        return Collection;
    }
}